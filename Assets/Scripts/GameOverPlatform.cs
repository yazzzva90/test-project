﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// платформа при прикосновении с которой игра заканчивается
/// </summary>
public class GameOverPlatform : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.childCount > 0)//У платформ нету дочерних обьектов)))Если не ней ненаходится игрок)
        {
            GameOver.End();
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.childCount > 0)//У платформ нету дочерних обьектов)))Если не ней ненаходится игрок)
        {
            GameOver.End();
        }

           
    }
}
