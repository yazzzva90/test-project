﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    //Платформа является начальной?
    public bool ground = false;
    //Метка первого приземления на платформу
    public bool completed = false;
    private Rigidbody2D rigidbody;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        PlayerControl.Instance.Hold(this);
        if (!completed)//Если персонаж первый раз приземлился на платформе
        {
            PlatformManager.Instance.NextPlatform();
            
            StartCoroutine(MoveToPoint(-1.5f, true));
            completed = true;
        }
        else if (!ground)//Если персонаж повторно преземлился на платформу и это не начальная платформа
        {
            PlayerControl.Instance.Lock();//Полная блокировка игрока
            StartCoroutine(MoveDown(4.0f));
        }
    }

    /// <summary>
    /// Двигает платформу в низ сцены и удаляет ее
    /// </summary>
    /// <param name="speed"></param>
    /// <returns></returns>
    public IEnumerator MoveDown(float speed = 2.0f)
    {
        rigidbody.velocity = new Vector2(0.0f, -speed);
        while (rigidbody.position.y > -7.0f)
            yield return null;
        Destroy(gameObject);
    }

    /// <summary>
    /// Двигает платформк в указаную точку
    /// </summary>
    /// <param name="point"></param>
    /// <param name="down">Если установленно в True активирует движение платформы в низ</param>
    /// <returns></returns>
    public IEnumerator MoveToPoint(float point, bool down = false)
    {
        rigidbody = GetComponent<Rigidbody2D>();
        rigidbody.velocity = new Vector2(0.0f, -3.0f);
        while(rigidbody.position.y > point)
        {
            yield return null;
        }
        rigidbody.velocity = Vector2.zero;
        rigidbody.position = new Vector2(0.0f, point);
        if(down)
            rigidbody.velocity = new Vector2(0.0f, -0.4f);
    }
}
