﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    //Величина на которую выростает сила прыжка при удержании за секунду
    public float addForce = 1.0f;
    //Максимальная сила прыжка
    public float maxForce = 10.0f;
    //Сила прыжка
    private float force = 0;
    //Полная блокировка управления игрока
    private bool lockP = false;
    private Rigidbody2D rigid;
    private PNGControl pngControl;
    private TriggerControl triggerControl;
    private static PlayerControl instance;

    private void Start()
    {
        instance = this;
        rigid = GetComponent<Rigidbody2D>();
        pngControl = GetComponentInChildren<PNGControl>();
        triggerControl = GetComponent<TriggerControl>();
        pngControl.SetMaxForce(maxForce);

    }

    public static PlayerControl Instance { get { return instance; } }

    private void Update()
    {
        if (Input.GetMouseButton(0))//Накопление силы прыжка при удержании
        {
            force += addForce * Time.deltaTime;
            if (force > maxForce) force = maxForce;
            pngControl.SetForce(force);//Сжатие изображение игрока в зависимости от силы
        }
        if (Input.GetMouseButtonUp(0))//Прыжок 
        {
            Unhold();//Открепить от платформы и заблокировать управление
            if (force < 1.3f) force = 1.3f;//Минимальная сила прыжка
            rigid.AddForce(new Vector2(0, force), ForceMode2D.Impulse);
            force = 0.0f;
            pngControl.SetDefault();
            triggerControl.OnTrigger();
        }
    }

    /// <summary>
    /// Открепляет персонажа от платформы и блокирует управление
    /// </summary>
    public void Unhold()
    {
        if (lockP) return;//Если включена полная блокировка, нельзя открепится от платформы
        transform.parent = null;
        rigid.isKinematic = false;
        enabled = false;
    }
    /// <summary>
    /// Закрепляет персонажа на платформе, и активируют управления
    /// </summary>
    /// <param name="platform"></param>
    public void Hold(Platform platform)
    {
        transform.parent = platform.transform;
         rigid.isKinematic = true;
        enabled = true;
    }

    /// <summary>
    /// Полная блокировка игрока, разблакировать можно только перезапуском сцены
    /// </summary>
    public void Lock()
    {
        lockP = true;
    }
}
