﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Переводит персонажа в состаяние треггер при движении в верх и обратно при движении в низ
/// </summary>
public class TriggerControl : MonoBehaviour
{
    private CapsuleCollider2D capsule;
    private Rigidbody2D rigid;
    private bool inTrigger;

    private void Start()
    {
        capsule = GetComponent<CapsuleCollider2D>();
        rigid = GetComponent<Rigidbody2D>();
        enabled = false;
    }

    /// <summary>
    /// Активировать тригер
    /// </summary>
    public void OnTrigger()
    {
        enabled = true;
        capsule.isTrigger = true;
    }

    private void FixedUpdate()
    {
        if(rigid.velocity.y <= 0.0f && !inTrigger)//Если игрок падает в низ И не в платформе
        {
            capsule.isTrigger = false;//Отключить трегер
            enabled = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        inTrigger = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        inTrigger = false;
    }
}
