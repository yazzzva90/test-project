﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformManager : MonoBehaviour
{
    public GameObject platformPref;
    //Текущая платформа на которой находится игрок, и платформа на которую должен запрыгнуть игрок
    public Platform currentP, nextP;

    private static PlatformManager instance;

    private void Start()
    {
        instance = this;
    }

    public static PlatformManager Instance { get { return instance; } }

    /// <summary>
    /// Добовляет новую платформу в игру
    /// </summary>
    public void NextPlatform()
    {
       StartCoroutine(currentP.MoveDown());
        currentP = nextP;
        nextP = Instantiate(platformPref, new Vector3(0.0f, 7.5f, 0.0f), Quaternion.identity).GetComponent<Platform>();
        StartCoroutine(nextP.MoveToPoint(2.5f));
    }
}
