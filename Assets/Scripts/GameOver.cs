﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    private static Canvas canvas;
    private void Start()
    {
        canvas = GetComponent<Canvas>();
        canvas.enabled = false;
    }

    /// <summary>
    /// Активация информации об оканчании игры и кнопки рестарт
    /// </summary>
    internal static void End()
    {
        Time.timeScale = 0.0f;
        canvas.enabled = true;
    }

    /// <summary>
    /// Перезапуск сцены
    /// </summary>
    public void Recovery()
    {
        SceneManager.LoadScene("SampleScene");
        Time.timeScale = 1.0f;
    }
}
