﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PNGControl : MonoBehaviour
{
    private float maxForce;

    /// <summary>
    /// Задает максимально возможную силу прыжка
    /// </summary>
    /// <param name="force"></param>
    public void SetMaxForce(float force)
    {
        maxForce = force;
    }

    /// <summary>
    /// Сжимает изображение в зависимости от пременненой силы
    /// </summary>
    /// <param name="force"></param>
    public void SetForce(float force)
    {
        float scale = force / maxForce;
       scale = 1.0f - scale * 0.5f;
        transform.localScale = new Vector3(1.0f, scale, 1.0f);
    }

    /// <summary>
    /// Возврощает изображение персонажа в нормальное состояния
    /// </summary>
    public void SetDefault()
    {
        transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
    }

}
